FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y dpkg-dev apt-utils gpg apache2 inotify-tools && apt-get clean
COPY entrypoint.sh /entrypoint.sh
COPY apt-regenerate /usr/local/bin/apt-regenerate
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/data/"]

EXPOSE 80
