# README #

Docker APT repository image with release file and signing capabilities.

### What is this repository for? ###

* Quickly set up and get an apt repository running for your projects
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* To build the image simply run **docker build .** on your machine
* Internally, it uses inotify-tools and apt-ftparchive to quickly regenerate Package and Release files upon copying to the target directory.
* To run the container use **docker run -d -v /path/to/your/packages/ -p <port>:80 lmiklosko/apt-repository**

* For Signing the release file, you need to import the certificate first. To do that, you need to import the certificate to a gpg, for example: **docker exec <container_id> gpg --import <private.key>**
If no certificate was provided, Release.gpg and InRelease files are not generated alongisde Release file

### Who do I talk to? ###

Created by: Lukas Miklosko <l(dot)miklosko(at)gmail(dot)com>
Feel free to contact me :)
