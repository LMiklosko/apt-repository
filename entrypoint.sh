#!/bin/bash

service apache2 start

while [ 1 ]; do
    inotifywait -r -e create -e modify ${1} 2>1 >/dev/null
    if [[ $(echo $?) -eq 0 ]]; then
        apt-regenerate ${1}
    fi
done
